﻿using System;

namespace DemoProject
{
    //Abstraction
    abstract class Car      //abstract parent class
    {
        public abstract void carName();     //abstract method

        public void capacity()
        {
            Console.WriteLine("Capacity: We can go 250km per charge");
        }
    
    } //End of abstract class car

    //Inheritance
    class Nexon : Car       //inheriting from abstract class
    {
        public override void carName()
        {
            Console.WriteLine("Car Name: Nexon EV");
        }

    }//End of Nexon Class

    //Encapsulation
    class Rectangle
    {
        public double length;
        public double width;

        public double GetArea()
        {
            return length * width;
        }
        public void Display()
        {
            Console.WriteLine("Length: {0}", length);
            Console.WriteLine("Width: {0}", width);
            Console.WriteLine("Area: {0}", GetArea());
        }
    }//End rectangle class


    //interface
    interface IAnimal
    {
        void AnimalSound();

    }
    class Dog : IAnimal
    {
        public void AnimalSound()
        {
            Console.WriteLine("The Dog says: bow bow");
        }
    }
    internal class Program
    {

        public static int staticVariable = 10;  //static variable
        public static void staticMethod()       //static method
        {
            staticVariable++;
            Console.WriteLine(staticVariable);
        }//End of static method

        //Polymorphism
        public int Add(int a, int b)
        {
            return a + b;
        }
        public int Add(int a, int b, int c)
        {
            return a + b + c;
        }

        static void Main(string[] args)
        {
            staticMethod();                         //calling static method 

            Program program = new Program();        //creating an object of Program class

            Console.WriteLine("Addition of Number is {0}", program.Add(10, 20));
            Console.WriteLine("Addition of Number is {0}", program.Add(10, 20, 30));

            Nexon abstractObj = new Nexon();        //creating an object of Nexon class
            abstractObj.carName();                  //call the abstract method
            abstractObj.capacity();                 //call the regular method

            Rectangle rectangle = new Rectangle();  //creating an object of Rectangle class
            rectangle.length = 5.5;
            rectangle.width = 2.5;
            rectangle.Display();

            Dog dog = new Dog();
            dog.AnimalSound();

            Console.ReadLine();
        }//End of main method
    }//End of Program class
    
}
